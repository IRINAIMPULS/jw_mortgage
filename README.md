# JW_mortgage

Write a python script that would scrape the reviews from the JustWebAgency Home Page.
https://www.justwebagency.com/
Reviews on the page:
Requirements:
Scrape the following data (if any of the fields is missing, it should be saved as a null):
Unique ID 
Review text
Stars (1-5)
Author name
Link to author avatar
Date of the review (try to get the actual date in UTC)
Save the reviews into a JSON file
Bonus: save into a relational SQL database (SQLite)
