from credentials import url, zip_file_name
from zip import get_df_zips, df_to_csv

if __name__ == '__main__':
    df_zip = get_df_zips(url)
    df_to_csv(df_zip, file_name=zip_file_name)

    print(df_zip.head(5))
