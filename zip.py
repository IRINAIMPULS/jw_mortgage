import csv
import os

import pandas as pd

from bs4 import BeautifulSoup
import requests


def remove_file(file_name):
    try:
        path = os.path.join(os.path.abspath(os.path.dirname(__file__)), file_name)
        os.remove(path)
    except FileNotFoundError as e:
        print(f'file not exist: {e}')
        exit()


def df_to_csv(df, file_name):
    try:
        with open(file_name, 'w', newline='') as file:
            df.to_csv(file)
    except Exception as e:
        print(e)
        exit()


def get_df_zips(url):
    try:
        html = requests.get(url).text
        soup = BeautifulSoup(html, "html.parser")
        tables = soup.find_all("table")
        index = 0
        files = list()
        for table in tables:
            f_name = str(index) + ".csv"
            files.append(f_name)
            with open(f_name, "w") as f:
                wr = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)
                wr.writerows([[td.text for td in row.find_all("td")] for row in table.find_all("tr")])
            index = index + 1
    except requests.exceptions.Timeout:
        print('# Probably the server is sick, you can try retray after N seconds')
        exit()
    except (requests.exceptions.TooManyRedirects, requests.exceptions.InvalidSchema):
        print('# Possibly invalid URL')
        exit()
    except requests.ConnectionError:
        print('# No connection to server')
        exit()

    try:
        df = pd.read_csv(files[0])
        df.dropna(axis='columns', how='all', inplace=True)
        df['zip'] = df.apply(lambda x: x['zips'].split(), axis=1)
        dd = df.explode('zip')
        for f in files:
            remove_file(f)
        return dd
    except IndexError:
        print(f'Data not received, please check the link: {url}')
        exit()
    except FileNotFoundError as e:
        print(f'zip-code file not received: {e}')
        exit()
